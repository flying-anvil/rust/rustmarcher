use crate::{raymarching::{primitives::Cube, SignedDistanceShape, advanced::Gyroid}, value::Float3};

use super::RaymarchingScene;

pub struct GyroidsAreFunScene {}

impl GyroidsAreFunScene {
    pub fn new() -> Self { Self {  } }
}

impl RaymarchingScene for GyroidsAreFunScene {
    fn get_scene_distance(&self, position: Float3, _time: f32) -> f32 {
        let cube = Cube::new(
            Float3::zero(),
            Float3::one(),
        );

        let gyroid = Gyroid::new();

        cube.distance_from(position).max(gyroid.distance_from(position * 4.0)) / 4.0
    }
}
