# RustMarcher

A software raymarcher written is rust.

Who needs a fast GPU anyways?

## Wishlist

- [ ] Create a *solid* way to handle video/animation rendering to files with progress indicator
- [ ] Animation renderer that produces an image sequence (or a video)
  - [ ] Parallelize it by working on multiple images at once (the frames are independent from each other)
  - [x] Parallelize it by working on multiple fragments/pixels at once
- [x] Add more primitive shapes
- [ ] Create a scene workflow, where "objects"/shapes and lights can be inserted into
- [ ] Make a TUI that can be used as a dashboard for the current render (like for progress, etc.)
- [ ] Make a GUI that can be used to compose a raymarching scene
  - [ ] Display the scene as a hierarchy
- [ ] Add the ability to create user-defined distance functions/scenes (that can also be used when composing scenes)
- [ ] Gyroids are always fun
- [ ] Implement common operators (in a nice and reusable way (orders might matter))
  - [ ] Moving/Translating
  - [ ] Rotation
  - [ ] Subtract/Cutting shapes
  - [ ] Adding/Combining shapes
  - [ ] Merging shapes
  - [ ] Blending shapes
