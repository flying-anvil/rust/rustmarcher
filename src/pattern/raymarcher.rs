use crate::{value::{Color, Float3, Float2}};
use crate::raymarching::scene::*;

use super::{Pattern, PatternInput};

pub struct Raymarcher {
    // variable distance function?
}

#[allow(unused)]
impl Pattern for Raymarcher {
    fn frag(&self, input: PatternInput) -> Color {
        let uv = input.normalized_uv_horizontal();

        // let camera_position = Float3::new(0.0, 2.0, 0.0);
        let camera_position = Float3::new(0.0, 2.0, -6.0);
        let ray_direction = Float3::new(uv.x, -uv.y - 0.2, 1.0).normalized(); // Why does y need to be inverted?

        let result =  self.raymarch(camera_position, ray_direction, input.time);

        // println!("{:#?}", hit);
        // std::process::exit(0);

        // return Color::visualize_hit(&result);
        // return Color::visualize_steps(result);
        // return Color::visualize_steps_no_misses(result);
        // return Color::visualize_hit_steps(result);
        // return Color::visualize_distance(result);
        // return Color::visualize_distance_hit(result);
        // return Color::visualize_hit_position(result);
        // return Color::visualize_hit_position_wrapped(result);
        // return Color::visualize_normal(result, input.time);

        if result.hit.is_none() {
            return Color::new_uniform(0.05);
        }

        let hit = result.hit.unwrap();

        let light_intensity = self.get_light(hit, input.time);
        let light_color = Float3::new(0.62, 0.35, 0.2).pow(0.2545);

        Color::from(light_color * light_intensity)
    }
}

#[derive(Debug)]
struct RaymarchResult {
    steps: u16,
    distance_from_origin: f32,
    hit: Option<RaymarchHit>,
}

#[derive(Debug)]
struct RaymarchHit {
    position: Float3,
}

impl RaymarchHit {
    fn normal(&self, time: f32) -> Float3 {
        Raymarcher::get_scene_normal(self.position, time)
    }
}

impl Raymarcher {
    const MAX_STEPS: u16 = 128;
    const SURFACE_THRESHOLD: f32 = 0.005;
    const MAX_DISTANCE: f32 = 64.0;

    fn raymarch(&self, ray_origin: Float3, ray_direction: Float3, time: f32) -> RaymarchResult {
        let mut distance_from_origin = 0.0;

        for i in 0..Self::MAX_STEPS {
            let position = ray_origin + (ray_direction * distance_from_origin);
            let distance_to_scene = Self::get_scene_distance(position, time);
            distance_from_origin += distance_to_scene;

            // println!("{distance_step}");

            // Close enough to count as hit.
            if distance_to_scene < Self::SURFACE_THRESHOLD {
                // The position variable is missing one iteration, so it needs to be recalculated.
                let hit_position = ray_origin + (ray_direction * distance_from_origin);
                return RaymarchResult {
                    steps: i,
                    distance_from_origin,
                    hit: Some(RaymarchHit {
                        position: hit_position,
                    }),
                }
            }

            // So far away that all hope is lost.
            if distance_from_origin > Self::MAX_DISTANCE {
                return RaymarchResult {
                    steps: i,
                    distance_from_origin: Self::MAX_DISTANCE, // Clamps the distance to the max (might prevent unexpected math)
                    hit: None,
                }
            }
        }

        // Exceeded limit of steps.
        RaymarchResult {
            steps: Self::MAX_STEPS,
            distance_from_origin,
            hit: None,
        }
    }

    fn get_scene_distance(position: Float3, time: f32) -> f32 {
        // let scene = TemplateScene::new();
        // let scene = ExhibitionScene::new();
        // let scene = OperationScene::new();
        let scene = GyroidsAreFunScene::new();

        scene.get_scene_distance(position, time)
    }

    fn get_scene_normal(position: Float3, time: f32) -> Float3 {
        let epsilon = Float2::new(0.01, 0.001);
        let distance = Self::get_scene_distance(position, time);

        Float3::new(
            distance - Self::get_scene_distance(position - epsilon.xyy(), time),
            distance - Self::get_scene_distance(position - epsilon.yxy(), time),
            distance - Self::get_scene_distance(position - epsilon.yyx(), time),
        ).normalized()
    }

    fn get_light(&self, hit: RaymarchHit, time: f32) -> f32 {
        let light_position = Float3::new(
            0.0 + (time * 0.7).sin() * 5.5,
            5.0,
            // 6.0 + (time * 0.7).cos() * 5.5,
            0.0 + (time * 0.7).cos() * 5.5,
        );

        let normal = hit.normal(time);
        let light_direction = (light_position - hit.position).normalized();
        let diffuse = light_direction.dot(normal).clamp(0.0, 1.0);

        // Shadows
        let light_distance = (light_position - hit.position).length();
        let scene_distance = self.raymarch(
            // Push the origin a bit outwards to prevent starting in a surface.
            hit.position + (normal * (Self::SURFACE_THRESHOLD * 1.025)),
            light_direction,
            time,
        ).distance_from_origin;

        if scene_distance < light_distance {
            diffuse * 0.1
        } else {
            diffuse
        }
    }
}

#[allow(unused)]
impl Color {
    fn visualize_hit(hit: &RaymarchResult) -> Self {
        match hit.hit {
            None => Self::new(0.075, 0.03, 0.04),
            Some(_) => Self::new(0.04, 0.25, 0.085),
        }
    }

    fn visualize_steps(hit: RaymarchResult) -> Self {
        Self::from(Float3::new(
            0.647,
            0.125,
            0.234,
        ) * (hit.steps as f32 / Raymarcher::MAX_STEPS as f32))
    }

    fn visualize_steps_no_misses(hit: RaymarchResult) -> Self {
        match hit.hit {
            None => Self::new(0.025, 0.025, 0.035),
            Some(_) => Self::from(Float3::new(
                0.647,
                0.125,
                0.234,
            ) * (hit.steps as f32 / Raymarcher::MAX_STEPS as f32)),
        }
    }

    fn visualize_hit_steps(hit: RaymarchResult) -> Self {
        Color::from(match hit.hit {
            None => Float3::new(0.647, 0.125, 0.234),
            Some(_) => Float3::new(0.125, 0.321, 0.647),
        } * (hit.steps as f32 / Raymarcher::MAX_STEPS as f32))

        // Self::lerp(
        //     Self::visualize_hit(&hit),
        //     Self::from(Float3::new(0.75, 0.25, 0.5) * (hit.steps as f32 / Raymarcher::MAX_STEPS as f32)),
        //     0.5,
        // )
    }

    fn visualize_distance(hit: RaymarchResult) -> Self {
        Self::new_uniform(hit.distance_from_origin / Raymarcher::MAX_DISTANCE)
    }

    fn visualize_distance_hit(hit: RaymarchResult) -> Self {
        match hit.hit {
            None => Self::new(0.995, 0.8, 0.84),
            Some(_) => Self::new_uniform(hit.distance_from_origin / Raymarcher::MAX_DISTANCE),
        }
    }

    fn visualize_hit_position(hit: RaymarchResult) -> Self {
        match hit.hit {
            None => Self::new(0.075, 0.03, 0.04),
            Some(hit) => Color::from(hit.position),
        }
    }

    fn visualize_hit_position_wrapped(hit: RaymarchResult) -> Self {
        match hit.hit {
            None => Self::new(0.075, 0.03, 0.04),
            Some(hit) => Color::from(hit.position).wrap(1.0),
        }
    }

    fn visualize_normal(hit: RaymarchResult, time: f32) -> Self {
        match hit.hit {
            None => Self::new(0.075, 0.03, 0.04),
            Some(hit) => Self::from(hit.normal(time)),
        }
    }

    fn wrap(&self, around: f32) -> Self {
        Self::new(
            // Wrap around 1 (% doesn't work with negative values).
            self.red.rem_euclid(around),
            self.green.rem_euclid(around),
            self.blue.rem_euclid(around),
        )
    }
}
