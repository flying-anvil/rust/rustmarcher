use super::{Float3, Float2};

#[derive(Debug, Clone, PartialEq)]
pub struct Color {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
}

impl Color {
    pub fn new(red: f32, green: f32, blue: f32) -> Self { Self { red, green, blue } }
    pub fn new_uniform(value: f32) -> Self { Self { red: value, green: value, blue: value } }

    pub fn luminance(&self) -> f32 {
        0.299 * self.red +
        0.587 * self.green +
        0.114 * self.blue
    }

    pub fn lerp(from: Self, to: Self, t: f32) -> Self {
        Self::new(
            Self::lerp_float(from.red, to.red, t),
            Self::lerp_float(from.green, to.green, t),
            Self::lerp_float(from.blue, to.blue, t),
        )
    }

    fn lerp_float(from: f32, to: f32, t: f32) -> f32 {
        let delta = to - from;
        from + (delta * t)
    }

    pub fn as_rgb_array_u8(&self) -> [u8; 3] {
        [
            (self.red * 255.0) as u8,
            (self.green * 255.0) as u8,
            (self.blue * 255.0) as u8,
        ]
    }

    pub fn as_rgb_array_u16(&self) -> [u16; 3] {
        [
            (self.red * 65535.0) as u16,
            (self.green * 65535.0) as u16,
            (self.blue * 65535.0) as u16,
        ]
    }
}

impl Default for Color {
    fn default() -> Self {
        Self::new_uniform(0.0)
    }
}

impl From<f32> for Color {
    fn from(value: f32) -> Self {
        Self::new_uniform(value)
    }
}

impl From<Float2> for Color {
    fn from(value: Float2) -> Self {
        Self::new(value.x, value.y, 0.0)
    }
}

impl From<Float3> for Color {
    fn from(value: Float3) -> Self {
        Self::new(value.x, value.y, value.z)
    }
}
