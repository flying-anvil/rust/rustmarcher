use crate::value::{Color, Float2};

mod mango_pattern;
mod template_pattern;
mod raymarcher;

pub use mango_pattern::MangoPattern;
pub use template_pattern::TemplatePattern;
pub use raymarcher::Raymarcher;

#[allow(unused)]
pub struct PatternInput {
    frag_coordinate: Float2,
    resolution: (u16, u16),
    resolution_float2: Float2,
    time: f32,
}

impl PatternInput {
    pub fn new(frag_coordinate: Float2, resolution: (u16, u16), resolution_float2: Float2, time: f32) -> Self { Self { frag_coordinate, resolution, resolution_float2, time } }

    pub fn normalized_uv_horizontal(&self) -> Float2 {
        (self.frag_coordinate - (0.5 * self.resolution_float2)) / self.resolution_float2.y
    }

    pub fn uv(&self) -> Float2 {
        Float2::new(
            self.frag_coordinate.x / self.resolution_float2.x - 1.0,
            self.frag_coordinate.y / self.resolution_float2.y - 1.0,
        )
    }
}

pub trait Pattern {
    fn frag(&self, input: PatternInput) -> Color;
}


