use crate::{value::Float3, raymarching::{primitives::Cube, SignedDistanceShape}};

use super::RaymarchingScene;

pub struct TemplateScene {}

impl TemplateScene {
    pub fn new() -> Self { Self {  } }
}

impl RaymarchingScene for TemplateScene {
    fn get_scene_distance(&self, position: Float3, time: f32) -> f32 {
        let cube = Cube::new(
            Float3::zero(),
            Float3::one() + ((time * 1.0).sin() * 0.35),
        );

        cube.distance_from(position)
    }
}
