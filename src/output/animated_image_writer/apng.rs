#![allow(unused)]

use std::{path::Path, fs::File, io::{BufWriter, Write}, time::Instant};

use png::{StreamWriter, Encoder, Writer};

use crate::{value::ColorBuffer, prelude::Result, output::{ColorBufferSequenceWriter, BitDepth}, runner::Runner, pattern::Pattern};

// This is a situation where I still fight against lifetimes. Life's sad :/
pub struct ApngWriter<'a> {
    bit_depth: BitDepth,
    path: String,
    // context: Option<Context<'a>>,
    stream: Option<StreamWriter<'a, BufWriter<File>>>,
}

struct Context<'a> {
    // encoder: Encoder<'a, BufWriter<File>>,
    // writer: Writer<BufWriter<File>>,
    stream: StreamWriter<'a, BufWriter<File>>,
    // stream: Option<StreamWriter<'a, BufWriter<File>>>,
}

impl<'a> Context<'a> {
    fn new(encoder: Encoder<'a, BufWriter<File>>) -> Self {
        let mut writer = encoder.write_header().unwrap();
        let stream = writer.into_stream_writer().unwrap();

        Self {
            // writer: writer,
            stream: stream,
            // stream: None,
        }
    }

    // fn get_stream(&'a mut self) -> &'a StreamWriter<'a, BufWriter<File>> {
    //     if self.stream.is_none() {
    //         let stream = self.writer.stream_writer().unwrap();
    //         self.stream = Some(stream);
    //     }

    //     return &self.stream.as_ref().unwrap();
    // }
}

impl ApngWriter<'_> {
    pub fn new(bit_depth: BitDepth, path: &str) -> Self { Self { bit_depth, path: path.to_owned(), stream: None } }
}

impl ColorBufferSequenceWriter for ApngWriter<'_> {
    fn prepare(&mut self, resolution: (u16, u16), total_frame_count: usize, _frame_time: f32) {
        let path = Path::new(&self.path);
        let file = File::create(path).unwrap();
        let writer = BufWriter::new(file);
        let mut encoder = png::Encoder::new(writer, resolution.0 as u32, resolution.1 as u32);

        encoder.set_color(png::ColorType::Rgb);
        encoder.set_depth(png::BitDepth::from(self.bit_depth));
        encoder.set_compression(png::Compression::Best);
        encoder.set_animated(total_frame_count as u32, 1).unwrap();

        // let context = Context {
        //     writer: &mut writer,
        //     stream,
        // };

        self.stream = Some(encoder.write_header().unwrap().into_stream_writer().unwrap());

        // self.context = Some(Context::new(encoder));
    }

    fn write_next<'a>(&'a mut self, buffer: &ColorBuffer, _frame_number: usize, frame_time: f32) -> Result<()> {
        let stream = self.stream.as_mut().unwrap();
        // stream.set_frame_delay(1, (1.0 / frame_time) as u16).unwrap();

        let data: Vec<u8> = match self.bit_depth {
            BitDepth::Eight => buffer.iter().flat_map(|color| color.as_rgb_array_u8()).collect(),
            BitDepth::Sixteen => buffer.iter().flat_map(|color| color.as_rgb_array_u16()).flat_map(|data| data.to_be_bytes()).collect(),
        };

        println!("Frame {} | Bytes: {}", _frame_number, data.len());
        stream.write(&data).unwrap();

        stream.flush().unwrap();

        Ok(())
    }

    fn finish(&mut self) {
        let stream = self.stream.as_mut().unwrap();
        stream.flush().unwrap();

        // let mut context = self.context.as_mut().unwrap();
        // let stream = context.get_stream();
    }
}

// pub struct ApngWrapper {
//     bit_depth: BitDepth,
//     path: String,
// }

// impl ApngWrapper {
//     pub fn new(bit_depth: BitDepth, path: String) -> Self { Self { bit_depth, path } }

//     pub fn do_stuff<P>(
//         &self,
//         runner: &Runner,
//         pattern: &P,
//         total_frame_count: usize,
//         frame_time: f32,
//     ) -> Result<()>
//     where
//         P: Pattern + std::marker::Sync,
//     {
//         let resolution = runner.resolution();
//         let path = Path::new(&self.path);
//         let file = File::create(path).unwrap();
//         let writer = BufWriter::new(file);
//         let mut encoder = png::Encoder::new(writer, resolution.0 as u32, resolution.1 as u32);

//         encoder.set_color(png::ColorType::Rgb);
//         encoder.set_depth(png::BitDepth::from(self.bit_depth));
//         encoder.set_compression(png::Compression::Best);
//         encoder.set_animated(total_frame_count as u32, 1).unwrap();

//         let mut durations_calculation = 0.0;
//         let mut animation_time = 0.0;

//         for i in 0..total_frame_count {
//             let start = Instant::now();
//             let buffer = runner.run_with_time(pattern, animation_time);
//             durations_calculation += start.elapsed().as_secs_f32();

//             animation_time += frame_time;
//         }


//         Ok(())
//     }
// }
