use std::ops::{Add, Sub, Mul, Div};

use super::Float3;

#[derive(Debug, Clone, Copy)]
pub struct Float2 {
    pub x: f32,
    pub y: f32,
}

impl Float2 {
    pub fn new(x: f32, y: f32) -> Self { Self { x, y } }
    pub fn new_uniform(value: f32) -> Self { Self { x: value, y: value } }

    pub fn length(&self) -> f32 {
        (
            self.x * self.x +
            self.y * self.y
        ).sqrt()
    }

    pub fn max_component_wise(&self, max: f32) -> Self {
        Self::new(
            self.x.max(max),
            self.y.max(max),
        )
    }

    pub fn xyx(&self) -> Float3 {
        Float3::new(self.x, self.y, self.x)
    }

    pub fn xyy(&self) -> Float3 {
        Float3::new(self.x, self.y, self.y)
    }

    pub fn yxy(&self) -> Float3 {
        Float3::new(self.y, self.x, self.y)
    }

    pub fn yyx(&self) -> Float3 {
        Float3::new(self.y, self.y, self.x)
    }
}

// ===== f32 ===================================================================

impl Add<f32> for Float2 {
    type Output = Self;

    fn add(self, rhs: f32) -> Self::Output {
        Self::new(self.x + rhs, self.y + rhs)
    }
}

impl Sub<f32> for Float2 {
    type Output = Self;

    fn sub(self, rhs: f32) -> Self::Output {
        Self::new(self.x - rhs, self.y - rhs)
    }
}

impl Mul<f32> for Float2 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}

impl Div<f32> for Float2 {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Self::new(self.x / rhs, self.y / rhs)
    }
}

impl Add<Float2> for f32 {
    type Output = Float2;

    fn add(self, rhs: Float2) -> Self::Output {
        Float2::new(self + rhs.x, self + rhs.y)
    }
}

impl Sub<Float2> for f32 {
    type Output = Float2;

    fn sub(self, rhs: Float2) -> Self::Output {
        Float2::new(self - rhs.x, self - rhs.y)
    }
}

impl Mul<Float2> for f32 {
    type Output = Float2;

    fn mul(self, rhs: Float2) -> Self::Output {
        Float2::new(self * rhs.x, self * rhs.y)
    }
}

impl Div<Float2> for f32 {
    type Output = Float2;

    fn div(self, rhs: Float2) -> Self::Output {
        Float2::new(self / rhs.x, self / rhs.y)
    }
}

// ===== Float2 ================================================================

impl Add<Float2> for Float2 {
    type Output = Self;

    fn add(self, rhs: Float2) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Sub<Float2> for Float2 {
    type Output = Self;

    fn sub(self, rhs: Float2) -> Self::Output {
        Self::new(self.x - rhs.x, self.y - rhs.y)
    }
}
