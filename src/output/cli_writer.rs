use std::io::{stdout, Write, stdin, Read};

use crate::{value::{ColorBuffer, Color}, prelude::Result};

use super::{ColorBufferWriter, ColorBufferSequenceWriter};

#[derive(Default)]
pub struct CliWriterLarge {}

#[derive(Default)]
pub struct CliWriterSmall {}

impl CliWriterLarge {
    pub fn new() -> Self { Self {} }
}

impl CliWriterSmall {
    pub fn new() -> Self { Self {} }
}

impl ColorBufferWriter for CliWriterLarge {
    fn write(&self, buffer: &ColorBuffer) -> Result<()> {
        enter_alternate_mode();

        write_large(buffer)?;

        wait_for_enter();
        exit_alternate_mode();

        Ok(())
    }
}

impl ColorBufferWriter for CliWriterSmall {
    fn write(&self, buffer: &ColorBuffer) -> Result<()> {
        enter_alternate_mode();

        write_small(buffer)?;

        wait_for_enter();
        exit_alternate_mode();

        Ok(())
    }
}

impl ColorBufferSequenceWriter for CliWriterLarge {
    fn prepare(&mut self, _resolution: (u16, u16), _total_frame_count: usize, _frame_time: f32) {
        enter_alternate_mode()
    }

    fn write_next(&mut self, buffer: &ColorBuffer, _frame_number: usize, _frame_time: f32) -> Result<()> {
        write_large(buffer)
    }

    fn finish(&mut self) {
        exit_alternate_mode()
    }
}

impl ColorBufferSequenceWriter for CliWriterSmall {
    fn prepare(&mut self, _resolution: (u16, u16), _total_frame_count: usize, _frame_time: f32) {
        enter_alternate_mode()
    }

    fn write_next(&mut self, buffer: &ColorBuffer, _frame_number: usize, _frame_time: f32) -> Result<()> {
        write_small(buffer)
    }

    fn finish(&mut self) {
        exit_alternate_mode()
    }
}

// impl Drop for CliWriterLarge {
//     fn drop(&mut self) {
//         exit_alternate_mode()
//     }
// }

// impl Drop for CliWriterSmall {
//     fn drop(&mut self) {
//         exit_alternate_mode()
//     }
// }

fn write_large(buffer: &ColorBuffer) -> Result<()> {
    let width = buffer.width();
    let height = buffer.height();

    let mut last_color = None;

    for y in 0..height {
        for x in 0..width {
            let color = buffer.get_color(x, y).unwrap();

            if Some(color) == last_color {
                print!("  ");
                continue;
            }

            let (red, green, blue) = color_to_ascii_components(color);
            print!("\x1B[48;2;{};{};{}m  ", red, green, blue);

            last_color = Some(color);
        }

        last_color = None;
        println!("\x1B[0m");
    }

    Ok(())
}

/// Only works with even amounts of rows.
/// If they are uneven, the last one is ignored!
fn write_small(buffer: &ColorBuffer) -> Result<()> {
    let width = buffer.width();
    let height = buffer.height();

    let mut last_color_top = None;
    let mut last_color_bottom = None;

    for y in (1..height).step_by(2) {
        for x in 0..width {
            let color_top = buffer.get_color(x, y - 1).unwrap();
            let color_bottom = buffer.get_color(x, y).unwrap();

            // All the same, don't change any colors
            if Some(color_top) == last_color_top && Some(color_bottom) == last_color_bottom {
                print!("▀");
                continue;
            }

            // Only change bottom/background color
            if Some(color_top) == last_color_top {
                let (red, green, blue) = color_to_ascii_components(color_bottom);
                print!("\x1B[48;2;{};{};{}m▀", red, green, blue);
                last_color_bottom = Some(color_bottom);
                continue;
            }

            // Only change top/foreground color
            if Some(color_top) == last_color_top {
                let (red, green, blue) = color_to_ascii_components(color_top);
                print!("\x1B[38;2;{};{};{}m▀", red, green, blue);
                last_color_top = Some(color_top);
                continue;
            }

            // Change both colors
            let (red_top, green_top, blue_top) = color_to_ascii_components(color_top);
            let (red_bottom, green_bottom, blue_bottom) = color_to_ascii_components(color_bottom);
            print!("\x1B[38;2;{};{};{};48;2;{};{};{}m▀", red_top, green_top, blue_top, red_bottom, green_bottom, blue_bottom);

            last_color_top = Some(color_top);
            last_color_bottom = Some(color_bottom);
        }

        print!("\x1B[0m");

        // Don't do this on the last row
        if y != height - 1 {
            last_color_top = None;
            last_color_bottom = None;
            println!();
        }
    }

    print!("\x1B[1H");
    stdout().flush().unwrap();
    Ok(())
}

fn color_to_ascii_components(color: &Color) -> (u8, u8, u8) {
    (
        // TODO: Check if this is an overflow or a saturation.
        (color.red * 255.0) as u8,
        (color.green * 255.0) as u8,
        (color.blue * 255.0) as u8,
    )
}

pub fn enter_alternate_mode() {
    // Enter alternate mode, move cursor to home (0, 0) and clear it
    print!("\x1B[?1049h\x1B[H\x1B[2J");
}

pub fn exit_alternate_mode() {
    print!("\x1B[?1049l");
}

// Is only a temporary "solution" for debugging.
#[allow(clippy::read_zero_byte_vec, clippy::unused_io_amount)]
pub fn wait_for_enter() {
    let mut input: Vec<u8> = Vec::new();
    stdin().read(&mut input).unwrap();
}
