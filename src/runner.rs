use rayon::prelude::{ParallelIterator, IntoParallelIterator, IndexedParallelIterator};

use crate::{value::{ColorBuffer, Float2, Color}, pattern::{Pattern, PatternInput}};

pub struct Runner {
    width: u16,
    height: u16,
}

impl Runner {
    pub fn new(width: u16, height: u16) -> Self { Self { width, height } }

    pub fn resolution(&self) -> (u16, u16) {
        (self.width, self.height)
    }

    pub fn run<P: Pattern + std::marker::Sync>(&self, pattern: &P) -> ColorBuffer {
        self.run_with_time(pattern, 0.0)
    }

    pub fn run_with_time<P: Pattern + std::marker::Sync>(&self, pattern: &P, time: f32) -> ColorBuffer {
        let length = ColorBuffer::calculate_required_length(self.width, self.height);

        let resolution = self.resolution();
        let resolution_float2 = Float2::new(self.width as f32, self.height as f32);

        let map_function = |index| {
            let (x, y) = ColorBuffer::any_index_to_position(self.width, index);

            pattern.frag(PatternInput::new(
                Float2::new(x as f32, y as f32),
                resolution,
                resolution_float2,
                time,
            ))
        };

        // Prevents re-allocating the vector.
        let mut colors: Vec<Color> = Vec::with_capacity(length);
        (0..length).into_par_iter().map(map_function).collect_into_vec(&mut colors); // Parallel
        // let colors = (0..length).into_iter().map(map_function).collect(); // Sequential

        ColorBuffer::wrap_buffer(self.width, self.height, colors).expect("Math should work out")
    }
}
