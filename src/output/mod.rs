mod cli_writer;
mod single_image_writer;
mod animated_image_writer;

pub use cli_writer::*;
pub use single_image_writer::SingleImageWriter;
pub use single_image_writer::Format as SingleImageFormat;

// pub use animated_image_writer::AnimatedImageWriter;
pub use animated_image_writer::Format as AnimatedImageFormat;
pub use animated_image_writer::ApngWriter;

use crate::{value::ColorBuffer, prelude::Result};

#[derive(Default, Debug, Clone, Copy)]
pub enum BitDepth {
    #[default]
    Eight = 8,
    Sixteen = 16,
}

impl From<BitDepth> for png::BitDepth {
    fn from(val: BitDepth) -> Self {
        match val {
            BitDepth::Eight => png::BitDepth::Eight,
            BitDepth::Sixteen => png::BitDepth::Sixteen,
        }
    }
}

pub trait ColorBufferWriter {
    fn write(&self, buffer: &ColorBuffer) -> Result<()>;
}

pub trait ColorBufferSequenceWriter {
    fn prepare(&mut self, resolution: (u16, u16), total_frame_count: usize, frame_time: f32);

    /// frame_time should be consistent across all frames, but that might change in the future.
    fn write_next(&mut self, buffer: &ColorBuffer, frame_number: usize, frame_time: f32) -> Result<()>;
    fn finish(&mut self);
}

#[derive(Default)]
pub struct NullWriter {}

impl NullWriter {
    pub fn new() -> Self { Self {} }
}

impl ColorBufferWriter for NullWriter {
    fn write(&self, buffer: &ColorBuffer) -> Result<()> {
        std::hint::black_box(buffer);
        Ok(())
    }
}

impl ColorBufferSequenceWriter for NullWriter {
    fn prepare(&mut self, _resolution: (u16, u16), _total_frame_count: usize, _frame_time: f32) {}

    fn write_next(&mut self, buffer: &ColorBuffer, _frame_number: usize, _frame_time: f32) -> Result<()> {
        std::hint::black_box(buffer);
        Ok(())
    }

    fn finish(&mut self) {}
}
