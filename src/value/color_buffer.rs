use crate::prelude::{Result, Error};

use super::Color;

#[derive(Debug)]
pub struct ColorBuffer {
    width: u16,
    height: u16,
    colors: Vec<Color>,
}

impl ColorBuffer {
    pub fn new(width: u16, height: u16) -> Self {
        Self {
            width,
            height,
            colors: vec![Color::default(); width as usize * height as usize],
        }
    }

    pub fn wrap_buffer(width: u16, height: u16, colors: Vec<Color>) -> Result<Self> {
        let required_length = Self::calculate_required_length(width, height);
        if colors.len() != required_length {
            return Err(Error::Generic(format!("colors must be of length {}, {} colors given", required_length, colors.len())));
        }

        Ok(Self { width, height, colors })
    }

    pub fn calculate_required_length(width: u16, height: u16) -> usize {
        width as usize * height as usize
    }

    pub fn replace_colors(&mut self, new_colors: Vec<Color>) -> Result<()> {
        if new_colors.len() != self.colors.capacity() {
            return Err(Error::Generic(format!("Size mismatch: old colors are {} long, replacement is {}", self.colors.capacity(), new_colors.len())));
        }

        self.colors = new_colors;

        Ok(())
    }

    pub fn width(&self) -> u16 {self.width}
    pub fn height(&self) -> u16 {self.height}

    pub fn position_to_index(&self, x: u16, y: u16) -> usize {
        y as usize * self.width as usize + x as usize
    }

    pub fn index_to_position(&self, index: usize) -> (u16, u16) {
        (
            (index % self.width as usize) as u16,
            (index / self.width as usize) as u16,
        )
    }

    pub fn any_index_to_position(width: u16, index: usize) -> (u16, u16) {
        (
                (index % width as usize) as u16,
                (index / width as usize) as u16,
            )
        }

    pub fn put_color(&mut self, x: u16, y: u16, color: Color) -> Result<()> {
        let index = self.position_to_index(x, y);
        if index > self.colors.capacity() {
            return Err(Error::Generic(format!("Position ({}, {}) is out-of-bounds", x, y)));
        }

        self.colors[index] = color;

        Ok(())
    }

    pub fn get_color(&self, x: u16, y: u16) -> Option<&Color> {
        let index = self.position_to_index(x, y);
        self.colors.get(index)
    }

    pub fn iter_indices(&self) -> std::ops::Range<usize> {
        0..self.colors.capacity()
    }

    pub fn iter(&self) -> std::slice::Iter<Color> {
        self.colors.iter()
    }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<Color> {
        self.colors.iter_mut()
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;
    use super::ColorBuffer;

    #[test]
    fn test_position_to_index() {
        let buffer = ColorBuffer::new(10, 5);

        assert_eq!(50, buffer.colors.capacity());

        assert_eq!(0, buffer.position_to_index(0, 0));
        assert_eq!(49, buffer.position_to_index(9, 4));

        assert_eq!(60, buffer.position_to_index(10, 5));
        assert!(buffer.position_to_index(10, 5) > buffer.colors.capacity());

        assert_eq!((0, 0), buffer.index_to_position(0));
        assert_eq!((9, 4), buffer.index_to_position(49));

        assert_eq!((3, 2), buffer.index_to_position(buffer.position_to_index(3, 2)));
    }
}
