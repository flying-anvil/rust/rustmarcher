use crate::{raymarching::{primitives::{Sphere, Capsule, Cylinder, Torus, Cube, Plane}, SignedDistanceShape}, value::Float3};

use super::RaymarchingScene;

pub struct ExhibitionScene {}

impl ExhibitionScene {
    pub fn new() -> Self { Self {} }
}

impl RaymarchingScene for ExhibitionScene {
    fn get_scene_distance(&self, position: crate::value::Float3, time: f32) -> f32 {
        // let time = 0.0f32; // Freeze the scene

        // let sphere = Sphere::new(Float3::new(
        //     0.0 + (time * 0.825234).sin() * 3.5,
        //     1.0 + ((time * 1.4).sin() * 0.5 + 0.5),
        //     6.0 + time.cos(),
        // ), 1.0);

        let sphere = Sphere::new(Float3::new(
            -1.75,
            0.4,
            3.25,
        ), 0.35 + ((time * 0.35).cos() * 0.1));

        let capsule = Capsule::new(
            Float3::new(3.0, 0.5, 6.0),
            Float3::new(3.1, 2.5, 6.0),
            0.5,
        );

        let cylinder = Cylinder::new(
            Float3::new(0.14, 0.3, 3.0),
            Float3::new(3.64, 0.3, 5.0),
            0.3,
        ); // .inflate(0.035);

        let torus = Torus::new(
            Float3::new(0.0, 0.5, 6.0),
            1.5,
            0.3,
        );

        let cube = Cube::new(
            Float3::new(-4.5, 0.8, 7.5),
            Float3::new(1.0, 0.8, 0.75),
        );

        let inflated_cube = Cube::new(
            Float3::new(-3.5, 0.0, 6.75),
            Float3::new(0.5, 0.3, 0.5),
        ).inflate(0.25);

        let plane = Plane::new(-0.0);

        plane.distance_from(position)
            .min(sphere.distance_from(position))
            .min(capsule.distance_from(position))
            .min(cylinder.distance_from(position))
            .min(torus.distance_from(position))
            .min(cube.distance_from(position))
            .min(inflated_cube.distance_from(position))
    }
}
