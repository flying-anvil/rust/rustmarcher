#![allow(unused)]
use std::{time::{Duration, Instant}, io::{stdout, Write}};

use rustmarcher::{runner::Runner, pattern::{MangoPattern, TemplatePattern, Pattern, Raymarcher}, output::{NullWriter, CliWriterSmall, CliWriterLarge, ColorBufferWriter, ColorBufferSequenceWriter, SingleImageWriter, SingleImageFormat, BitDepth, ApngWriter}};
use termsize::Size;

fn main() {
    let terminal_size = termsize::get().unwrap_or(Size { rows: 40, cols: 80});
    // rayon::ThreadPoolBuilder::new().num_threads(2).build_global().unwrap(); // For debugging, use only 2 thread.

    // let pattern = MangoPattern {};
    // let pattern = TemplatePattern {};
    let pattern = Raymarcher {};

    // let runner = Runner::new(64, 64);
    // let runner = Runner::new(1920, 1080);
    // let runner = Runner::new(1024, 1024);
    // let runner = Runner::new(terminal_size.cols / 2, terminal_size.rows); // Large pixels
    let runner = Runner::new(terminal_size.cols, (terminal_size.rows) * 2); // Small pixels

    // let writer = NullWriter::new();
    // let writer = CliWriterLarge::new();
    let mut writer = CliWriterSmall::new();
    // let mut writer = ApngWriter::new(BitDepth::Eight, "./out/out.apng");
    // let mut writer = Apng2Writer::new(BitDepth::Eight, "./out/out.apng");
    // let writer = SingleImageWriter::new(SingleImageFormat::PNG(BitDepth::Eight), "./out/out.png");

    // single_frame(&runner, &pattern, &writer);
    animation_wrapper(&runner, &pattern, &mut writer);
    single_frame(&Runner::new(1920, 1080), &pattern, &SingleImageWriter::new(SingleImageFormat::PNG(BitDepth::Eight), "./out/out.png"));

    // println!("{buffer:#?}");
}

fn single_frame<P, W>(runner: &Runner, pattern: &P, writer: &W)
where
    P: Pattern + std::marker::Sync,
    W: ColorBufferWriter,
{
    let start = Instant::now();
    let buffer = runner.run_with_time(pattern, 21.75);
    let duration_calculation = start.elapsed().as_secs_f32();

    writer.write(&buffer);
    let duration_with_render = start.elapsed().as_secs_f32();

    println!(
        "Calculated 1 frame in {:.4} s ({:.2} FPS)",
        duration_calculation,
        1.0 / duration_calculation,
    );

    print!("\x1B[4m");

    println!(
        "Rendered   1 frame in {:.4} s ({:.2} FPS)     \x1B[0m",
        duration_with_render,
        1.0 / duration_with_render,
    );
}

fn animation_wrapper<P, W>(runner: &Runner, pattern: &P, writer: &mut W)
where
    P: Pattern + std::marker::Sync,
    W: ColorBufferSequenceWriter,
{
    let frame_count = 60;
    let frame_time = 1.0 / 10.0;
    let start = Instant::now();
    let (durations_calculation, durations_with_render) = animation_frame(
        runner,
        pattern,
        writer,
        frame_count,
        frame_time,
        1.0,
        true,
    );

    let duration_with_throttle = start.elapsed().as_secs_f32();

    println!(
        "Calculated {} frames in {:.2} s ({:.2} FPS)",
        frame_count,
        durations_calculation,
        frame_count as f32 / durations_calculation,
    );

    println!(
        "Rendered   {} frames in {:.2} s ({:.2} FPS)",
        frame_count,
        durations_with_render,
        frame_count as f32 / durations_with_render,
    );

    println!(
        "Displayed  {} frames in {:.2} s ({:.2} FPS) | Target FPS: {}",
        frame_count,
        duration_with_throttle,
        frame_count as f32 / duration_with_throttle,
        1.0 / frame_time,
    );
}

fn animation_frame<P, W>(runner: &Runner, pattern: &P, writer: &mut W, frame_count: usize, frame_time: f32, speed: f32, throttle: bool) -> (f32, f32)
where
    P: Pattern + std::marker::Sync,
    W: ColorBufferSequenceWriter,
{
    writer.prepare(runner.resolution(), frame_count, frame_time);

    let mut durations_calculation = 0.0;
    let mut durations_with_render = 0.0;

    let mut animation_time = 0.0;
    for i in 0..frame_count {
        let start = Instant::now();
        let buffer = runner.run_with_time(pattern, animation_time);
        durations_calculation += start.elapsed().as_secs_f32();

        writer.write_next(&buffer, i, frame_time);

        // TODO: Remove debug progress display
        print!("\x1B[1H\r{}", i + 1);
        stdout().flush().unwrap();

        let duration_with_render = start.elapsed().as_secs_f32();

        animation_time += frame_time * speed;
        durations_with_render += duration_with_render;

        if throttle {
            let sleep_duration = (frame_time - duration_with_render) * 0.925;
            if sleep_duration > 0.0 {
                std::thread::sleep(Duration::from_secs_f32(sleep_duration));
            }
        }
    }

    writer.finish();

    (durations_calculation, durations_with_render)
}
