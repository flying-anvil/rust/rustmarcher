use std::{path::Path, fs::File, io::BufWriter};

use crate::value::ColorBuffer;

use super::{ColorBufferWriter, BitDepth};

#[derive(Debug)]
pub enum Format {
    PNG(BitDepth),
    JPG,
}

impl Default for Format {
    fn default() -> Self { Self::PNG(BitDepth::Eight) }
}

pub struct SingleImageWriter {
    format: Format,
    path: String,
}

impl SingleImageWriter {
    pub fn new(format: Format, path: &str) -> Self { Self { format, path: path.to_owned() } }

    fn encode_png(&self, buffer: &ColorBuffer, bit_depth: BitDepth) -> Result<(), crate::prelude::Error> {
        let path = Path::new(&self.path);
        let file = File::create(path).unwrap();
        let writer = BufWriter::new(file);
        let mut encoder = png::Encoder::new(writer, buffer.width() as u32, buffer.height() as u32);

        encoder.set_color(png::ColorType::Rgb);
        encoder.set_depth(png::BitDepth::from(bit_depth));
        encoder.set_compression(png::Compression::Best);

        let mut encoder = encoder.write_header().unwrap();

        let data: Vec<u8> = match bit_depth {
            BitDepth::Eight => buffer.iter().flat_map(|color| color.as_rgb_array_u8()).collect(),
            BitDepth::Sixteen => buffer.iter().flat_map(|color| color.as_rgb_array_u16()).flat_map(|data| data.to_be_bytes()).collect(),
        };

        encoder.write_image_data(&data).unwrap();

        Ok(())
    }
}

impl ColorBufferWriter for SingleImageWriter {
    fn write(&self, buffer: &ColorBuffer) -> crate::prelude::Result<()> {
        match self.format {
            Format::PNG(bit_depth) => self.encode_png(buffer, bit_depth),
            Format::JPG => todo!(),
        }
    }
}
