use super::BitDepth;

mod apng;
// mod apng2;

pub use self::apng::ApngWriter;
// pub use apng2::Apng2Writer;

#[derive(Debug)]
pub enum Format {
    APNG(BitDepth),
}

impl Default for Format {
    fn default() -> Self { Self::APNG(BitDepth::Eight) }
}

// pub struct AnimatedImageWriter {
//     format: Format,
//     path: String,
// }

// impl AnimatedImageWriter {
//     pub fn new(format: Format, path: &str) -> Self { Self { format, path: path.to_owned() } }
// }

// impl ColorBufferSequenceWriter for AnimatedImageWriter {
//     fn prepare(&self, resolution: (u16, u16), total_frame_count: usize, frame_time: f32) {
//         match self.format {
//             Format::APNG(bit_depth) => self.prepare_apng(resolution, bit_depth, total_frame_count, frame_time),
//         }
//     }

//     fn write_next(&self, frame_number: usize, buffer: &ColorBuffer, frame_time: f32) -> Result<()> {
//         match self.format {
//             Format::APNG(bit_depth) => self.encode_apng_next(buffer, bit_depth, frame_number, frame_time),
//         }
//     }

//     fn finish(&self) {
//         match self.format {
//             Format::APNG(_bit_depth) => self.finish_apng(),
//         }
//     }
// }
