use crate::value::Color;

use super::{Pattern, PatternInput};

pub struct MangoPattern {}

impl Pattern for MangoPattern {
    fn frag(&self, input: PatternInput) -> Color {
        let uv = input.normalized_uv_horizontal();

        Color::new(
            uv.x,
            uv.y,
            0.0,
        )
    }
}
