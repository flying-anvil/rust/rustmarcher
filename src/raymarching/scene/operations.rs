use crate::{raymarching::{primitives::{Sphere, Torus, Plane, Cube}, SignedDistanceShape}, value::Float3};

use super::RaymarchingScene;

pub struct OperationScene {}

impl OperationScene {
    pub fn new() -> Self { Self {} }
}

impl RaymarchingScene for OperationScene {
    fn get_scene_distance(&self, position: crate::value::Float3, _time: f32) -> f32 {
        let sphere_main = Sphere::new(
            Float3::new(2.0, -0.5, 6.0),
            2.5,
        );

        let torus_top = Torus::new(
            Float3::new(2.0, 1.75, 6.0),
            1.25,
            0.3,
        );

        let torus_middle = Torus::new(
            Float3::new(2.0, 0.75, 6.0),
            2.0,
            0.3,
        );

        let torus_bottom = Torus::new(
            Float3::new(2.0, 0.1, 6.0),
            2.6,
            0.25,
        );

        let sphere_cut = torus_top.distance_from(position)
            .min(torus_middle.distance_from(position))
            .min(torus_bottom.distance_from(position));

        let plane = Plane::new(-0.0);
        let sphere_hole = Sphere::new(Float3::new(
            -2.25,
            1.5,
            4.3,
        ), 1.75);

        let milled_cube = Cube::new(
            Float3::new(-4.5, 1.0, 7.0),
            Float3::one(),
        );

        let cube_cut = Sphere::new(
            Float3::new(-4.5, 1.0, 7.0),
            1.45,
        );

        let cube_anti_cut = Sphere::new(
            Float3::new(-4.5, 1.0, 7.0),
            1.6,
        );

        let cornered_offset = Float3::new(2.5, 0.05, 0.25);

        (plane.distance_from(position).max(-sphere_hole.distance_from(position)))
            // .min(sphere_main.distance_from(position).max(-cut)) // Cut sphere only
            .min(sphere_main.distance_from(position)).max(-sphere_cut) // Cut whole scene
            .min(milled_cube.distance_from(position).max(cube_cut.distance_from(position)) - 0.004)
            .min(milled_cube.inflate(0.1).distance_from(position - cornered_offset).max(-cube_anti_cut.distance_from(position - cornered_offset)))
    }
}
