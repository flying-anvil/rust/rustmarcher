use crate::value::Float3;

mod template;
mod exhibition;
mod operations;
mod gyroids_are_fun;

pub use template::TemplateScene;
pub use exhibition::ExhibitionScene;
pub use operations::OperationScene;
pub use gyroids_are_fun::*;

pub trait RaymarchingScene {
    fn get_scene_distance(&self, position: Float3, time: f32) -> f32;
}
