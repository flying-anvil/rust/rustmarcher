use crate::value::{Color, Float3};

use super::{Pattern, PatternInput};

pub struct TemplatePattern {}

impl Pattern for TemplatePattern {
    fn frag(&self, input: PatternInput) -> Color {
        let uv = input.normalized_uv_horizontal();
        let col = (uv.xyx() + input.time + Float3::new(0.0, 2.0, 4.0)).cos();

        Color::from(0.5 + 0.5 * col)
    }
}
