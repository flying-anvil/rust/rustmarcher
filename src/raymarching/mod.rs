use crate::value::Float3;

use self::primitives::InflatedShape;

pub mod primitives;
pub mod advanced;
pub mod scene;

pub trait SignedDistanceShape {
    fn distance_from(&self, position: Float3) -> f32;

    fn inflate(self, inflation: f32) -> InflatedShape<Self>
    where
        Self: Sized,
    {
        InflatedShape::new(self, inflation)
    }
}
