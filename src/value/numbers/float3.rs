use std::ops::{Add, Sub, Mul, Div};

use super::Float2;

#[derive(Debug, Clone, Copy)]
pub struct Float3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Float3 {
    pub fn new(x: f32, y: f32, z: f32) -> Self { Self { x, y, z } }
    pub fn zero() -> Float3 { Self::new(0.0, 0.0,  0.0) }
    pub fn one() -> Float3 { Self::new(1.0, 1.0,  1.0) }

    pub fn length(&self) -> f32 {
        (
            self.x * self.x +
            self.y * self.y +
            self.z * self.z
        ).sqrt()
    }

    pub fn sin(&self) -> Float3 {
        Self::new(
            self.x.sin(),
            self.y.sin(),
            self.z.sin(),
        )
    }

    pub fn cos(&self) -> Self {
        Self::new(
            self.x.cos(),
            self.y.cos(),
            self.z.cos(),
        )
    }

    pub fn normalized(&self) -> Self {
        self / self.length()
    }

    pub fn dot(&self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub fn abs(&self) -> Self {
        Self::new(
            self.x.abs(),
            self.y.abs(),
            self.z.abs(),
        )
    }

    pub fn max_component_wise(&self, max: f32) -> Self {
        Self::new(
            self.x.max(max),
            self.y.max(max),
            self.z.max(max),
        )
    }

    pub fn pow(&self, power: f32) -> Self {
        Self::new(
            self.x.powf(power),
            self.y.powf(power),
            self.z.powf(power),
        )
    }

    pub fn xz(&self) -> Float2 {
        Float2::new(self.x, self.z)
    }

    pub fn zxy(&self) -> Self {
        Self::new(self.z, self.x, self.y)
    }
}

// ===== f32 ===================================================================

impl Add<f32> for Float3 {
    type Output = Self;

    fn add(self, rhs: f32) -> Self::Output {
        Self::new(self.x + rhs, self.y + rhs, self.z + rhs)
    }
}

impl Sub<f32> for Float3 {
    type Output = Self;

    fn sub(self, rhs: f32) -> Self::Output {
        Self::new(self.x - rhs, self.y - rhs, self.z - rhs)
    }
}

impl Mul<f32> for Float3 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs, self.z * rhs)
    }
}

impl Div<f32> for Float3 {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Float3::new(self.x / rhs, self.y / rhs, self.z / rhs)
    }
}

impl Add<Float3> for f32 {
    type Output = Float3;

    fn add(self, rhs: Float3) -> Self::Output {
        Float3::new(self + rhs.x, self + rhs.y, self + rhs.z)
    }
}

impl Sub<Float3> for f32 {
    type Output = Float3;

    fn sub(self, rhs: Float3) -> Self::Output {
        Float3::new(self - rhs.x, self - rhs.y, self - rhs.z)
    }
}

impl Mul<Float3> for f32 {
    type Output = Float3;

    fn mul(self, rhs: Float3) -> Self::Output {
        Float3::new(self * rhs.x, self * rhs.y, self * rhs.z)
    }
}

impl Div<Float3> for f32 {
    type Output = Float3;

    fn div(self, rhs: Float3) -> Self::Output {
        Float3::new(self / rhs.x, self / rhs.y, self / rhs.z)
    }
}

impl Add<f32> for &Float3 {
    type Output = Float3;

    fn add(self, rhs: f32) -> Self::Output {
        Float3::new(self.x + rhs, self.y + rhs, self.z + rhs)
    }
}

impl Sub<f32> for &Float3 {
    type Output = Float3;

    fn sub(self, rhs: f32) -> Self::Output {
        Float3::new(self.x - rhs, self.y - rhs, self.z - rhs)
    }
}

impl Mul<f32> for &Float3 {
    type Output = Float3;

    fn mul(self, rhs: f32) -> Self::Output {
        Float3::new(self.x * rhs, self.y * rhs, self.z * rhs)
    }
}

impl Div<f32> for &Float3 {
    type Output = Float3;

    fn div(self, rhs: f32) -> Self::Output {
        Float3::new(self.x / rhs, self.y / rhs, self.z / rhs)
    }
}

impl Add<&Float3> for f32 {
    type Output = Float3;

    fn add(self, rhs: &Float3) -> Self::Output {
        Float3::new(self + rhs.x, self + rhs.y, self + rhs.z)
    }
}

impl Sub<&Float3> for f32 {
    type Output = Float3;

    fn sub(self, rhs: &Float3) -> Self::Output {
        Float3::new(self - rhs.x, self - rhs.y, self - rhs.z)
    }
}

impl Mul<&Float3> for f32 {
    type Output = Float3;

    fn mul(self, rhs: &Float3) -> Self::Output {
        Float3::new(self * rhs.x, self * rhs.y, self * rhs.z)
    }
}

impl Div<&Float3> for f32 {
    type Output = Float3;

    fn div(self, rhs: &Float3) -> Self::Output {
        Float3::new(self / rhs.x, self / rhs.y, self / rhs.z)
    }
}

// ===== Float3 ================================================================

impl Add for Float3 {
    type Output = Float3;

    fn add(self, rhs: Self) -> Self::Output {
        Float3::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub for Float3 {
    type Output = Float3;

    fn sub(self, rhs: Self) -> Self::Output {
        Float3::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Add for &Float3 {
    type Output = Float3;

    fn add(self, rhs: Self) -> Self::Output {
        Float3::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub for &Float3 {
    type Output = Float3;

    fn sub(self, rhs: Self) -> Self::Output {
        Float3::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Add<Float3> for &Float3 {
    type Output = Float3;

    fn add(self, rhs: Float3) -> Self::Output {
        Float3::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub<Float3> for &Float3 {
    type Output = Float3;

    fn sub(self, rhs: Float3) -> Self::Output {
        Float3::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Add<&Float3> for Float3 {
    type Output = Float3;

    fn add(self, rhs: &Float3) -> Self::Output {
        Float3::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub<&Float3> for Float3 {
    type Output = Float3;

    fn sub(self, rhs: &Float3) -> Self::Output {
        Float3::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}
