use crate::value::Float3;

use super::SignedDistanceShape;

pub struct Gyroid {

}

impl Gyroid {
    pub fn new() -> Self { Self {} }
}

impl SignedDistanceShape for Gyroid {
    fn distance_from(&self, position: Float3) -> f32 {
        Float3::dot(
            &position.sin(),
            position.zxy().cos(),
        )
    }
}
