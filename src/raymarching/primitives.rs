use crate::value::{Float3, Float2};

use super::SignedDistanceShape;

// ===== Sphere =================================================================
pub struct Sphere {
    position: Float3,
    radius: f32,
}

impl Sphere {
    pub fn new(position: Float3, radius: f32) -> Self { Self { position, radius } }
}

impl SignedDistanceShape for Sphere {

    fn distance_from(&self, position: Float3) -> f32 {
        (position - self.position).length() - self.radius
    }
}

// ===== Plane =================================================================
pub struct Plane {
    height: f32,
}

impl Plane {
    pub fn new(height: f32) -> Self { Self { height } }
}

impl SignedDistanceShape for Plane {
    fn distance_from(&self, position: Float3) -> f32 {
        position.y - self.height
    }
}

// ===== Capsule ===============================================================
pub struct Capsule {
    point_a: Float3,
    point_b: Float3,
    radius: f32,
}

impl Capsule {
    pub fn new(point_a: Float3, point_b: Float3, radius: f32) -> Self { Self { point_a, point_b, radius } }
}

impl SignedDistanceShape for Capsule {
    fn distance_from(&self, position: Float3) -> f32 {
        let ab = self.point_b - self.point_a;
        let ap = position - self.point_a;
        let t = (ab.dot(ap) / ab.dot(ab)).clamp(0.0, 1.0);

        let closest = self.point_a + (t * ab);
        (position - closest).length() - self.radius
    }
}

// ===== Cylinder ==============================================================
pub struct Cylinder {
    point_a: Float3,
    point_b: Float3,
    radius: f32,
}

impl Cylinder {
    pub fn new(point_a: Float3, point_b: Float3, radius: f32) -> Self { Self { point_a, point_b, radius } }
}

impl SignedDistanceShape for Cylinder {
    fn distance_from(&self, position: Float3) -> f32 {
        let ab = self.point_b - self.point_a;
        let ap = position - self.point_a;
        let t = ab.dot(ap) / ab.dot(ab);

        let closest = self.point_a + (t * ab);
        let x = (position - closest).length() - self.radius;
        let y = ((t - 0.5).abs() - 0.5) * ab.length();
        let exterior = Float2::new(x, y).max_component_wise(0.0).length();

        // Clippy says to use `x.clamp(y, 0.0)`, but it panics if max < min, which does happen here.
        #[allow(clippy::manual_clamp)]
        let interior = x.max(y).min(0.0);

        exterior + interior
    }
}

// ===== Torus =================================================================
pub struct Torus {
    position: Float3,
    radius: f32,
    thickness: f32,
}

impl Torus {
    pub fn new(position: Float3, radius: f32, thickness: f32) -> Self { Self { position, radius, thickness } }
}

impl SignedDistanceShape for Torus {
    fn distance_from(&self, position: Float3) -> f32 {
        // Transform space-time (the math assumes the torus to be at the origin, so we can move the origin around).
        let position = position - self.position;

        let x = position.xz().length() - self.radius;
        Float2::new(x, position.y).length() - self.thickness
    }
}

// ===== Cube ==================================================================
pub struct Cube {
    position: Float3,
    size: Float3,
}

impl Cube {
    pub fn new(position: Float3, size: Float3) -> Self { Self { position, size } }
}

impl SignedDistanceShape for Cube {
    fn distance_from(&self, position: Float3) -> f32 {
        // Simple but with incorrect internal distance.
        // Transform space-time (the math assumes the torus to be at the origin, so we can move the origin around).
        let position = position - self.position;
        (position.abs() - self.size).max_component_wise(0.0).length()

        // let position = (position - self.position).abs() - self.size;
        // position.max_component_wise(0.0).length() + Float3::min_component_wise(Float3::max, max)
    }
}

// ===== InflatedShape =========================================================
pub struct InflatedShape<S: SignedDistanceShape> {
    shape: S,
    inflation: f32,
}

impl<S: SignedDistanceShape> InflatedShape<S> {
    pub fn new(shape: S, inflation: f32) -> Self { Self { shape, inflation } }
}

impl<S: SignedDistanceShape> SignedDistanceShape for InflatedShape<S> {
    fn distance_from(&self, position: Float3) -> f32 {
        self.shape.distance_from(position) - self.inflation
    }
}
