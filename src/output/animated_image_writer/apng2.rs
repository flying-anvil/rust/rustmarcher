use std::{io::{Write, BufWriter}, fs::File};

use crate::{value::ColorBuffer, prelude::Result, output::{ColorBufferSequenceWriter, BitDepth}};

pub struct Apng2Writer<'a> {
    bit_depth: BitDepth,
    path: String,
    encoder: Option<apng::Encoder<'a, BufWriter<File>>>,
}

impl Apng2Writer<'_> {
    pub fn new(bit_depth: BitDepth, path: &str) -> Self { Self { bit_depth, path: path.to_owned(), encoder: None } }
}

impl ColorBufferSequenceWriter for Apng2Writer<'_> {
    fn prepare(&mut self, resolution: (u16, u16), total_frame_count: usize, _frame_time: f32) {
        let mut out = BufWriter::new(File::create(&self.path).unwrap());

        let config = apng::create_config(&Vec::new(), Some(1)).unwrap();
        let encoder = apng::Encoder::new(&mut out, config).unwrap();
        self.encoder = Some(encoder);
    }

    fn write_next<'a>(&'a mut self, buffer: &ColorBuffer, _frame_number: usize, frame_time: f32) -> Result<()> {
        let data: Vec<u8> = match self.bit_depth {
            BitDepth::Eight => buffer.iter().flat_map(|color| color.as_rgb_array_u8()).collect(),
            BitDepth::Sixteen => buffer.iter().flat_map(|color| color.as_rgb_array_u16()).flat_map(|data| data.to_be_bytes()).collect(),
        };

        println!("Frame {} | Bytes: {}", _frame_number, data.len());
        let mut encoder = self.encoder.unwrap();

        let image = apng::PNGImage { width: buffer.width() as u32, height: buffer.height() as u32, data: data, color_type: png::ColorType::Rgb, bit_depth: png::BitDepth::from(self.bit_depth) };
        let frame = apng::Frame { delay_num: Some(1), delay_den: Some(20), ..Default::default() };
        encoder.write_frame(&image, frame);

        Ok(())
    }

    fn finish(&mut self) {
        self.encoder.unwrap().finish_encode();
    }
}
